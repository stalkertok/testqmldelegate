#include "model.h"
#include <QFile>
#include <QStringList>
#include <QDebug>

QHash<int, QByteArray> Model::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[UserIdRole] = "userId";
    roles[UserNameRole] = "userName";
    roles[PhoneNumberRole] = "phoneNumber";
    return roles;
}

Model::Model(QObject *parent):QAbstractListModel (parent){

}

void Model::fromFile(const QString &filename){

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug()<<"file "+filename+" is not open";
        return;
    }

    QByteArray line;
    QList<QByteArray> personString;
    auto readLines =1;

    while (!file.atEnd()) {
        line = file.readLine();

        personString=line.split(splitter);

        bool ok;
        personString[0].toInt(&ok);

        if (!ok)
            qDebug()<<"id bad format";

        if (personString.count()>3){
            beginInsertRows(QModelIndex(), rowCount(), rowCount());
            persons << Person(personString[0].toInt(),personString[1],personString[2]);
            endInsertRows();
        }else {
            qDebug()<<"bad format line"+QString::number(readLines)+" in file";
        }

        readLines++;

    }


}

void Model::addPerson(const Person &person){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    persons << person;
    endInsertRows();
}

int Model::rowCount(const QModelIndex & parent) const {
    Q_UNUSED(parent);
    return persons.count();
}

QVariant Model::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= persons.count())
        return QVariant();

    const Person &person = persons[index.row()];

    switch (role) {

    case UserIdRole:
        return person.getUserId();
    case UserNameRole:
        return person.getUserName();
    case PhoneNumberRole:
        return person.getPhoneNumber();

    }

    return QVariant();
}




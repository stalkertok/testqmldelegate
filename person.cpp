#include "person.h"

Person::Person(int userId, const QString &userName, const QString &phoneNumber):
                userId(userId),userName(userName),phoneNumber(phoneNumber){

}

int Person::getUserId() const{
    return userId;
}

void Person::setUserId(int value){
    userId = value;
}

QString Person::getUserName() const{
    return userName;
}

void Person::setUserName(const QString &value){
    userName = value;
}

QString Person::getPhoneNumber() const{
    return phoneNumber;
}

void Person::setPhoneNumber(const QString &value){
    phoneNumber = value;
}

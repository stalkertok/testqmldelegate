#ifndef MODEL_H
#define MODEL_H

#include <QAbstractListModel>
#include "person.h"

constexpr char splitter = ';';

class Model: public QAbstractListModel{
    Q_OBJECT
public:
    enum AnimalRoles {
        UserIdRole = Qt::UserRole + 1,
        UserNameRole,
        PhoneNumberRole
    };

    Model(QObject *parent = 0);

    void fromFile(const QString& filename);

    void addPerson(const Person &person);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    QHash<int, QByteArray> roleNames() const;

private:
    QList<Person> persons;
};



#endif // MODEL_H

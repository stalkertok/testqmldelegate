#ifndef PERSON_H
#define PERSON_H

#include <QString>


class Person{

public:
    Person(int userId, const QString& userName, const QString& phoneNumber);
    int getUserId() const;
    void setUserId(int value);

    QString getUserName() const;
    void setUserName(const QString &value);

    QString getPhoneNumber() const;
    void setPhoneNumber(const QString &value);

private:
    int userId;
    QString userName;
    QString phoneNumber;
};

#endif // PERSON_H

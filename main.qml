import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Person")

    TableView {
        id: tableView
        anchors.fill: parent

        TableViewColumn {title: "userId"; role: "userId"; }
        TableViewColumn {title: "userName"; role: "userName";}
        TableViewColumn {title: "phoneNumber"; role: "phoneNumber";}

        model: _Model
    }
}
